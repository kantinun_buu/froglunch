function Frog(){
    var tFrog = new Sprite(scene, "frog.png", 60, 60);
    //properties
    tFrog.setSpeed(0);
    tFrog.setAngle(0);
    tFrog.setPosition(320, 200);

    //methods
    tFrog.motionFrog = function()
    {
        if (keysDown[K_W] || keysDown[K_UP]){
            if(this.speed < 10){
                this.changeSpeedBy(1);
            }
        }
        if (keysDown[K_S]  || keysDown[K_DOWN]){
            if(this.speed > -3){
                this.changeSpeedBy(-1);
            }
        }
        if (keysDown[K_A]  || keysDown[K_LEFT]){
            this.changeAngleBy(-5);
        }
        if (keysDown[K_D]  || keysDown[K_RIGHT]){
            this.changeAngleBy(5);
        }
        
    }

    return tFrog;
}

function Fly(){
    var tFly = new Sprite(scene, "fly.png", 30, 30);
    tFly.setSpeed(15);
    tFly.setPosition(Math.random() * this.cWidth, Math.random() * this.cHeight);

    tFly.motionFly = function(){
        newDir = (Math.random()*90)-45;
        tFly.changeAngleBy(newDir);
    }

    tFly.respawn = function(){
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY);
    }

    return tFly;
}